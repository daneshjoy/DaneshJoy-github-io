---
permalink: /matlab/dsp
title: "پردازش سیگنال دیجیتال در متلب "
excerpt: "Digital Signal Processing in Matlab"
sitemap: true
comments: off
tags:
    - متلب
    - پردازش سیگنال
    - آموزش
---

**کدهای کلاس پردازش سیگنال با متلب**

## دانلود کدها
- صفحه گیتهاب کدها: <a  href="http://github.com/DaneshJoy/matlabdsp" target="_blank"><img src="{{ '/assets/images/github.png' | relative_url }}" width="30px" title="MatlabDSP on Github" alt="MatlabDSP on Github"></a><br/>
- دانلود مستقیم کدها: <a  href="https://github.com/DaneshJoy/matlabdsp/archive/master.zip"><img src="{{ '/assets/images/download.png' | relative_url }}" width="40px" title="دانلود" alt="دانلود"></a><br/>
- *&#x202b;برای اجرای کدها باید ورژن متلب بالاتر از 2016a باشد*

-------------------------------------

## لینک ویدیوهای آموزشی برای هر کد

-------------------------------------

### 1- ایجاد و آنالیز سیگنال سینوسی در حوزه زمان و فرکانس

| **SinePlay** | **یوتیوب** | **آپارت** |
| قسمت 1: ایجاد سیگنال سینوسی در متلب | <a href="https://youtu.be/Qfki1clmsPs" target="_blank"><img src="{{ '/assets/images/youtube.png' | relative_url }}" width="30px" title="یوتیوب" alt="یوتیوب"></a> | <a href="https://www.aparat.com/v/kl1eE" target="_blank"><img src="{{ '/assets/images/aparat.png' | relative_url }}" width="30px" title="اپارات" alt="اپارات"></a> |
| قسمت 2: نکات محاسبه تبدیل فوریه در متلب | <a href="https://youtu.be/27PBMl7l6sk" target="_blank"><img src="{{ '/assets/images/youtube.png' | relative_url }}" width="30px" title="یوتیوب" alt="یوتیوب"></a> | <a href="https://www.aparat.com/v/oYOB7" target="_blank"><img src="{{ '/assets/images/aparat.png' | relative_url }}" width="30px" title="اپارات" alt="اپارات"></a> |
| قسمت 3: تشخیص پیک ساده و نمایش نتایج | <a href="https://youtu.be/yYXC3fu3Dpo" target="_blank"><img src="{{ '/assets/images/youtube.png' | relative_url }}" width="30px" title="یوتیوب" alt="یوتیوب"></a> | <a href="https://www.aparat.com/v/B63Lo" target="_blank"><img src="{{ '/assets/images/aparat.png' | relative_url }}" width="30px" title="اپارات" alt="اپارات"></a> |
| قسمت 4: تاثیر نویز در حوزه های زمان و فرکانس | <a href="https://youtu.be/4_hRsE792kA" target="_blank"><img src="{{ '/assets/images/youtube.png' | relative_url }}" width="30px" title="یوتیوب" alt="یوتیوب"></a> | <a href="https://www.aparat.com/v/iZROx" target="_blank"><img src="{{ '/assets/images/aparat.png' | relative_url }}" width="30px" title="اپارات" alt="اپارات"></a> |


-------------------------------------

### 2- آنالیز سیگنال قلبی ECG

| **ECG PeakAnalysis** | **یوتیوب** | **آپارت** |
|  &#x202b; قسمت 1: حذف ترند سیگنال ECG | <a href="https://youtu.be/c-qYhtngcjU" target="_blank"><img src="{{ '/assets/images/youtube.png' | relative_url }}" width="30px" title="یوتیوب" alt="یوتیوب"></a> | <a href="https://www.aparat.com/v/ihHDt" target="_blank"><img src="{{ '/assets/images/aparat.png' | relative_url }}" width="30px" title="اپارات" alt="اپارات"></a> |
|  &#x202b; قسمت 2: تشخیص پیک های R و S در سیگنال ECG | <a href="https://youtu.be/ASyLt3KfTBk" target="_blank"><img src="{{ '/assets/images/youtube.png' | relative_url }}" width="30px" title="یوتیوب" alt="یوتیوب"></a> | <a href="https://www.aparat.com/v/qyzd2" target="_blank"><img src="{{ '/assets/images/aparat.png' | relative_url }}" width="30px" title="اپارات" alt="اپارات"></a> |
|  &#x202b; قسمت 3: حذف نویز فرکانس بالا و تشخیص پیک Q | <a href="https://youtu.be/a4GSkAt845E" target="_blank"><img src="{{ '/assets/images/youtube.png' | relative_url }}" width="30px" title="یوتیوب" alt="یوتیوب"></a> | <a href="https://www.aparat.com/v/I8BkE" target="_blank"><img src="{{ '/assets/images/aparat.png' | relative_url }}" width="30px" title="اپارات" alt="اپارات"></a> |
| قسمت 4: گزارش خطای تشخیص پیک و مشخصات زمانی پیک ها | <a href="https://youtu.be/BenNWPkpSoM" target="_blank"><img src="{{ '/assets/images/youtube.png' | relative_url }}" width="30px" title="یوتیوب" alt="یوتیوب"></a> | <a href="https://www.aparat.com/v/zr1Tv" target="_blank"><img src="{{ '/assets/images/aparat.png' | relative_url }}" width="30px" title="اپارات" alt="اپارات"></a> |


-------------------------------------

<p align="center">
  <a href="https://daneshjoy.ir">
    <img src="{{ '/assets/images/DaneshJoy.png' | relative_url }}" width="300px" title="DaneshJoy" alt="DaneshJoy"> 
  </a>
</p>

<div class="well">
<div class="rw-ui-container"></div>
</div>

