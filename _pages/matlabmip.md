---
permalink: /matlab/mip
title: "پردازش تصاویر پزشکی با متلب"
excerpt: "Medical Image Processing in Matlab"
sitemap: true
comments: off
tags:
    - متلب
    - پردازش تصویر
    - پردازش تصاویر پزشکی
    - آموزش
---

**کدهای کلاس پردازش تصاویر پزشکی با متلب**

## دانلود کدها

- صفحه گیتهاب کدها: <a  href="https://github.com/DaneshJoy/matlabmip" target="_blank"><img src="{{ '/assets/images/github.png' | relative_url }}" width="30px" title="MatlabDSP on Github" alt="MatlabDSP on Github"></a><br/>
- دانلود مستقیم کدها: <a  href="https://github.com/DaneshJoy/matlabmip/archive/master.zip"><img src="{{ '/assets/images/download.png' | relative_url }}" width="40px" title="دانلود" alt="دانلود"></a><br/>


-------------------------------------

## لینک ویدیوهای آموزشی

-------------------------------------
  
### 1- جلسه اول

| **موضوع** | **یوتیوب** | **آپارت** |
| قسمت 0: مقدمه | <a href="https://youtu.be/b927dRAXwNY" target="_blank"><img src="{{ '/assets/images/youtube.png' | relative_url }}" width="30px" title="یوتیوب" alt="یوتیوب"></a> | <a href="http://aparat.com/v/I4S8d" target="_blank"><img src="{{ '/assets/images/aparat.png' | relative_url }}" width="30px" title="اپارات" alt="اپارات"></a> |
| قسمت 1: فرمت های مرسوم تصاویر پزشکی | <a href="https://youtu.be/JTSoTTFx1CE" target="_blank"><img src="{{ '/assets/images/youtube.png' | relative_url }}" width="30px" title="یوتیوب" alt="یوتیوب"></a> | <a href="http://aparat.com/v/68AKo" target="_blank"><img src="{{ '/assets/images/aparat.png' | relative_url }}" width="30px" title="اپارات" alt="اپارات"></a> |
| قسمت 2: صفحه ها و جهت های آناتومیکی | <a href="https://youtu.be/WHgDRzEVgMQ" target="_blank"><img src="{{ '/assets/images/youtube.png' | relative_url }}" width="30px" title="یوتیوب" alt="یوتیوب"></a> | <a href="http://aparat.com/v/4HMFO" target="_blank"><img src="{{ '/assets/images/aparat.png' | relative_url }}" width="30px" title="اپارات" alt="اپارات"></a> |
| قسمت 3: نحوه اضافه کردن تولباکس ها به متلب | <a href="https://youtu.be/plIgjus59gM" target="_blank"><img src="{{ '/assets/images/youtube.png' | relative_url }}" width="30px" title="یوتیوب" alt="یوتیوب"></a> | <a href="http://aparat.com/v/p3V2t" target="_blank"><img src="{{ '/assets/images/aparat.png' | relative_url }}" width="30px" title="اپارات" alt="اپارات"></a> |
| قسمت 4: نحوه استفاده از تولباکس های معرفی شده | <a href="https://youtu.be/ZQbXGj571ng" target="_blank"><img src="{{ '/assets/images/youtube.png' | relative_url }}" width="30px" title="یوتیوب" alt="یوتیوب"></a> | <a href="http://aparat.com/v/NvdKb" target="_blank"><img src="{{ '/assets/images/aparat.png' | relative_url }}" width="30px" title="اپارات" alt="اپارات"></a> |
| قسمت 5: نحوه نمایش تصاویر | <a href="https://youtu.be/3-BFaQU2zjQ" target="_blank"><img src="{{ '/assets/images/youtube.png' | relative_url }}" width="30px" title="یوتیوب" alt="یوتیوب"></a> | <a href="http://aparat.com/v/c03zb" target="_blank"><img src="{{ '/assets/images/aparat.png' | relative_url }}" width="30px" title="اپارات" alt="اپارات"></a> |

-------------------------------------

### 2- جلسه دوم

| **موضوع** | **یوتیوب** | **آپارت** |
| &#x202b; قسمت 1: خواندن تصاویر DICOM | <a href="https://youtu.be/4iv-lLXwBXc" target="_blank"><img src="{{ '/assets/images/youtube.png' | relative_url }}" width="30px" title="یوتیوب" alt="یوتیوب"></a> | <a href="http://aparat.com/v/auOYe" target="_blank"><img src="{{ '/assets/images/aparat.png' | relative_url }}" width="30px" title="اپارات" alt="اپارات"></a> |
| &#x202b; قسمت 2:  نمایش تصاویر با colormap | <a href="https://youtu.be/MXkqdvfbYDE" target="_blank"><img src="{{ '/assets/images/youtube.png' | relative_url }}" width="30px" title="یوتیوب" alt="یوتیوب"></a> | <a href="http://aparat.com/v/0ogUO" target="_blank"><img src="{{ '/assets/images/aparat.png' | relative_url }}" width="30px" title="اپارات" alt="اپارات"></a> |
| &#x202b; قسمت 3: ذخیره تصاویر DICOM | <a href="https://youtu.be/XIlDMjVjE7g" target="_blank"><img src="{{ '/assets/images/youtube.png' | relative_url }}" width="30px" title="یوتیوب" alt="یوتیوب"></a> | <a href="http://aparat.com/v/hpAzq" target="_blank"><img src="{{ '/assets/images/aparat.png' | relative_url }}" width="30px" title="اپارات" alt="اپارات"></a> |
| قسمت 4: بریدن تصاویر با استفاده از ماسک | <a href="https://youtu.be/1VMKtzPR_JU)" target="_blank"><img src="{{ '/assets/images/youtube.png' | relative_url }}" width="30px" title="یوتیوب" alt="یوتیوب"></a> | <a href="http://aparat.com/v/PzB5k" target="_blank"><img src="{{ '/assets/images/aparat.png' | relative_url }}" width="30px" title="اپارات" alt="اپارات"></a> |
| قسمت 5: اعمال ماسک بر روی تصویر | <a href="https://youtu.be/5s175rTiXvs" target="_blank"><img src="{{ '/assets/images/youtube.png' | relative_url }}" width="30px" title="یوتیوب" alt="یوتیوب"></a> | <a href="http://aparat.com/v/tQb63" target="_blank"><img src="{{ '/assets/images/aparat.png' | relative_url }}" width="30px" title="اپارات" alt="اپارات"></a> |
| قسمت 6: نمایش سه بعدی تصاویر | <a href="https://youtu.be/FOn0twfgYO4" target="_blank"><img src="{{ '/assets/images/youtube.png' | relative_url }}" width="30px" title="یوتیوب" alt="یوتیوب"></a> | <a href="http://aparat.com/v/2LPuv" target="_blank"><img src="{{ '/assets/images/aparat.png' | relative_url }}" width="30px" title="اپارات" alt="اپارات"></a> |

-------------------------------------

### 3- جلسه سوم

| **موضوع** | **یوتیوب** | **آپارت** |
| &#x202b; قسمت 1: خواندن و نوشتن فرمت آنالایز HDR | <a href="https://youtu.be/V9IH3G7E-Ps" target="_blank"><img src="{{ '/assets/images/youtube.png' | relative_url }}" width="30px" title="یوتیوب" alt="یوتیوب"></a> | <a href="http://aparat.com/v/0jNRf" target="_blank"><img src="{{ '/assets/images/aparat.png' | relative_url }}" width="30px" title="اپارات" alt="اپارات"></a> |
| &#x202b; قسمت 2: نمایش تصاویر بر روی هم Overlay | <a href="https://youtu.be/FEpj4mdzUJk" target="_blank"><img src="{{ '/assets/images/youtube.png' | relative_url }}" width="30px" title="یوتیوب" alt="یوتیوب"></a> | <a href="http://aparat.com/v/It0eO" target="_blank"><img src="{{ '/assets/images/aparat.png' | relative_url }}" width="30px" title="اپارات" alt="اپارات"></a> |
| &#x202b; قسمت 3: آستانه گذاری تصاویر Thresholding | <a href="https://youtu.be/o-Aiy4eltKQ" target="_blank"><img src="{{ '/assets/images/youtube.png' | relative_url }}" width="30px" title="یوتیوب" alt="یوتیوب"></a> | <a href="http://aparat.com/v/C1tBW" target="_blank"><img src="{{ '/assets/images/aparat.png' | relative_url }}" width="30px" title="اپارات" alt="اپارات"></a> |
| قسمت 4: بهبود کنتراست تصاویر | <a href="https://youtu.be/khtKq8tNHsU" target="_blank"><img src="{{ '/assets/images/youtube.png' | relative_url }}" width="30px" title="یوتیوب" alt="یوتیوب"></a> | <a href="http://aparat.com/v/eAkjS" target="_blank"><img src="{{ '/assets/images/aparat.png' | relative_url }}" width="30px" title="اپارات" alt="اپارات"></a> |
| قسمت 5: اعمال فیلتر بر روی تصاویر | <a href="https://youtu.be/Xzv3n0mmck0" target="_blank"><img src="{{ '/assets/images/youtube.png' | relative_url }}" width="30px" title="یوتیوب" alt="یوتیوب"></a> | <a href="http://aparat.com/v/pZzwB" target="_blank"><img src="{{ '/assets/images/aparat.png' | relative_url }}" width="30px" title="اپارات" alt="اپارات"></a> |
| قسمت 6: نرمالیزه کردن تصاویر | <a href="https://youtu.be/MeiAmX2YNp0" target="_blank"><img src="{{ '/assets/images/youtube.png' | relative_url }}" width="30px" title="یوتیوب" alt="یوتیوب"></a> | <a href="http://aparat.com/v/z5fBV" target="_blank"><img src="{{ '/assets/images/aparat.png' | relative_url }}" width="30px" title="اپارات" alt="اپارات"></a> |

-------------------------------------

<p align="center">
  <a href="https://daneshjoy.ir">
    <img src="../../assets/images/DaneshJoy.png" width="300px" title="DaneshJoy" alt="DaneshJoy"> 
  </a>
</p>

<div class="well">
<div class="rw-ui-container"></div>
</div>
